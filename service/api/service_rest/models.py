from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, blank=True, null=True, unique=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField(blank=True, null=True)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return f"vin: {self.vin}"

class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_technicians", kwargs={"id": self.id})

class Appointment(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    owner_name = models.TextField()
    date = models.DateField()
    time = models.TimeField()
    vip = models.BooleanField(default=False)
    reason = models.TextField()
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"id": self.id})
    