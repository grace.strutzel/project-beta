from django.urls import path


from .views import (
    api_technicians,
    api_show_technicians,
    api_history,
    api_appointments,
    api_show_appointments,
)

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("technicians/<int:id>/", api_show_technicians, name="api_show_technicians"),
    path("appointments/<str:id>/", api_history, name="api_hostory"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("appointments/<int:id>/", api_show_appointments, name="api_show_appointments"),
]
