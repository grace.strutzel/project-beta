# CarCar

Team:

* Grace - Service
* Sharda - Sales

## Service Application
The Inventory section manages the manufacturers, vehicle models, and automobiles in the system. It allows the user to:
- add new manufacturers 
- access to the vehicle models list and add new vehicle models
- access to the automobiles list and add new automobiles to your inventory


The AutoSales section allows you to do the following:
- add a new sales rep to your system by filling out a form with their name and empliyee number
- add new customers by filling otu a form with their name, address, and phone number
- add a new sale records by filling out the form
- a user can access the autosales records list  that contains the salesperson’s name, employee number, the purchaser’s name, the automobile VIN, and the price of each sale.


## How to Run this Application:
git clone the repository and cd into the folder https://gitlab.com/grace.strutzel/project-beta.git
build and start containers to start up docker
    docker volume create beta-data
    docker-compose build
    docker-compose up
Register apps and create url
Create model and register
make migrations
create views and register the urls
Set up insomnia with the appropriate urls to GET, POST, DELETE each view (look at api documentation to set this up)
Create the poller to pull any other VOs from outside apps
Create the react frontend
Create list and forms
route them through apps
Add them into the navigation in order to navigate the page (look at services to see the services)

## Application Diagram



1. Fork the repository 
2. Copy path to clone with HTTPS
4. Build your containers in docker
8. Check (http://localhost:3000/) to access the web app.
9. Use the navbar at the top to navigate throughout the website
10. You can click on any of the forms to add a manufacturer, model, or automobile to update the inventory.

## Application Diagram
Image uploaded to the repo 


## Services
Inventory:
Shows a list of manufacturers
Create a manufacturer form
Shows a list of vehicle models
Create a vehicle model form
Shows a list of automobiles in inventory
Create an automobile in inventory form

This can either be a separate section with the services, their URLs, and ports listed here or you can include it in the application diagram
GHI: localhost:3000 (React Front End)
    List Appointments http://localhost:3000/appointments
    - user can view the owner, vin, date, time, if it is vip it will return true, reason, technician, and they can complete or cancel the appointment
    Create Appointments http://localhost:3000/appointments/new
    -user can enter vin, name, date, time, and choose a technician in order to create an appointment
    Vin history appointments http://localhost:3000/appointments/history (Show specific vin history)
    - view owner, vin, date, time, reason, technician
    List technicians http://localhost:3000/technicians
    - user can view technician name and employee number as well as delete them
    Create technicians http://localhost:3000/technicians/new
    - user can create a new technician
    
    Inventory
    Manufacturer http://localhost:3000/manufacturers
    - manufacturer and delete
    - new manufacturer http://localhost:3000/manufacturers/new
    Models http://localhost:3000/models
    - name, manufacturer, picture, and delete
    - new model http://localhost:3000/models/new
    Automobiles http://localhost:3000/automobiles
    - model, color, year, vin
    -new automobiles http://localhost:3000/automobiles/new

Sales:
List of Sales Reps
Create a new sales rep record
Create a new customer file
Create a new salesrecord
List all autosales
List sales rep autosales history

Services:
List of technocians
Create a new technician file
Service appoinment list
Create a new service appointment


## API Documentation

Technician: 
GET http://localhost:8080/api/technicians/ (List of technicians)
POST http://localhost:8080/api/technicians/ (Create technician)
GET, PUT, DELETE http://localhost:8080/api/technicians/1/ (Delete, update, show technician)

Appointment: GET POST http://localhost:8080/api/appointments/ (List and create appointment)
    GET, PUT, DELETE http://localhost:8080/api/appointments/1/ (Delete, update, show appointment)
    
Appointment: 
GET POST http://localhost:8080/api/appointments/ (List and create appointment)
GET, PUT, DELETE http://localhost:8080/api/appointments/1/ (Delete, update, show appointment)

Saleperson:
- GET Salesperson History http://localhost:8090/api/salesrephistory/
- GET List Salesperson http://localhost:8090/api/salesperson/
- POST Create Salesperson  http://localhost:8090/api/salesperson/new

Auto Sales:
- GET List Auto Sale Record http://localhost:8090/api/autosales/
- POST Create Auto Sale Record http://localhost:8090/api/autosales/new

Customer:
- POST Create Customer http://localhost:8090/api/customer/new

## Value Objects
AutomobileVO - import_href, color, year, vin
created to retrieve the vin in order to compare in the automobile list to check if vin numbers match to return if the customer deserves vip status
If you didn't identify the VOs in your diagram, then identify them here.

AutomobileVO: 
- a value object that polls the VIN, id_num and import_href from the Automobile model in the Inventory microservice.
