import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
                <p className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" id="a1">
                  <b>Inventory</b>
                </p>
                <ul className="dropdown-menu">
                  <li><NavLink className="dropdown-item" to="/manufacturers">Manufacturers List</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/models">Models List</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/automobiles">Automobile List</NavLink></li>
                </ul>
            </li>
            <li className="nav-item dropdown">
                <p className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" id="a1">
                  <b>Service</b>
                </p>
                <ul className="dropdown-menu">
                  <li><NavLink className="dropdown-item" to="/technicians">Technicians List</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/technicians/new">New Technicians</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/appointments">Appointments List</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/appointments/new">New Appointment</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/appointments/history">Appointment history</NavLink></li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <p className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" id="a3">
                  <b>Sales</b>
                </p>
                <ul className="dropdown-menu">
                  <li><NavLink className="dropdown-item" to="/autosales">Auto Sales Records List</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/salesperson">Salesperson List</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/salesrephistory">Sales Rep History</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/customers">New Customer</NavLink></li>
                </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
