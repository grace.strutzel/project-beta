import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechForm from './Service/TechForm';
import TechList from './Service/TechList';
import AppointmentList from './Service/AppointmentList';
import AppointmentForm from './Service/AppointmentForm';
import AppointmentHistory from './Service/AppointmentHistory';
import ModelsList from './Inventory/ModelsList';
import ManufacturersList from './Inventory/Inventoryold/ManufacturersList';
import AutoList from './Inventory/AutoList';
import SalespersonList from './Sales/SalespersonList';
import NewModel from './Inventory/Inventoryold/NewModel';
import ListModel from './Inventory/ListModel';
import AutoSalesRecordForm from './Sales/AutoSalesRecordForm';
import CustomerForm from './Sales/CustomerForm';
import NewAutomobile from './Inventory/Inventoryold/NewAutomobile';
import SalespersonForm from './Sales/SalespersonForm';
import NewManufacturer from './Inventory/Inventoryold/NewManufacturer';
import ListAutomobile from './Inventory/Inventoryold/ListAutomobile';
import ListManufacturer from './Inventory/Inventoryold/ListManufacturer';
import SalesRepHistory from './Sales/SalesRepHistory';
import AutoSalesRecordList from './Sales/AutoSalesRecordList';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians">
            <Route index element={<TechList/>} />
            <Route path="new" element={<TechForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentList/>} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentHistory />} />
          </Route>
          <Route path="models">
            <Route index element={<ModelsList/>} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturersList/>} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutoList/>} />
          </Route>
          <Route path="/" element={<MainPage />} />
          <Route path="/autosales">
            <Route path="" element={<AutoSalesRecordList />}/>
            <Route path="new" element={<AutoSalesRecordForm/>}/>
          </Route>
          <Route path="/salesperson">
            <Route path="" element={<SalespersonList/>}/>
            <Route path="new" element={<SalespersonForm/>}/>
            <Route path="SalesRepHistory" element={<SalesRepHistory/>}/>
          </Route>
          <Route path="/customers">
            <Route path="new" element={<CustomerForm/>}/>
            <Route path="" element={<CustomerForm/>}/>
          </Route>
          <Route path="/manufacturers">
            <Route path="" element={<ListManufacturer/>}/>
            <Route path="new" element={<NewManufacturer/>}/>
          </Route>
          <Route path="/models">
            <Route path="" element={<ListModel/>} />
            <Route path="new" element={<NewModel/>} />
          </Route>
          <Route path="/automobiles">
            <Route path="" element={<ListAutomobile/>} />
            <Route path="new" element={<NewAutomobile/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;


{/* function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/autosales">
            <Route path="" element={<AutoSalesRecordList />}/>
            <Route path="new" element={<AutoSalesRecordForm/>}/>
          </Route>
          <Route path="/salesperson">
            <Route path="" element={<SalespersonList/>}/>
            <Route path="new" element={<SalespersonForm/>}/>
            <Route path="SalespersonHistory" element={<SalespersonHistory/>}/>
          </Route>
          <Route path="/customers">
            <Route path="new" element={<CustomerForm/>}/>
            <Route path="" element={<CustomerForm/>}/>
          </Route>
          <Route path="/manufacturers">
            <Route path="" element={<ListManufacturer/>}/>
            <Route path="new" element={<NewManufacturer/>}/>
          </Route>
          <Route path="/models">
            <Route path="" element={<ListModel/>} />
            <Route path="new" element={<NewModel/>} />
          </Route>
          <Route path="/automobiles">
            <Route path="" element={<ListAutomobile/>} />
            <Route path="new" element={<NewAutomobile/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App; */}
