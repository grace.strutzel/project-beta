import React from 'react';

class AppointmentForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vin: '',
            owner_name: '',
            date: '',
            time: '',
            reason: '',
            technician: '',
            technicians: []
        };
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.technicians;
        console.log(data)

        const appUrl = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(appUrl, fetchConfig);
        if (response.ok) {
            const newApp = await response.json();
            console.log(newApp)

            const cleared = {
                vin: '',
                owner_name: '',
                date: '',
                time: '',
                reason: '',
                technician: '',
            }
            this.setState(cleared)
        }
    }

    async componentDidMount() {
        const url = "http://localhost:8080/api/technicians/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            this.setState({ technicians: data.techs })
        }
    }



    handleChange(event) {
        const value = event.target.value
        this.setState({[event.target.id]: value})
    }

    render() {
        return (
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create a new Appointment</h1>
                            <form onSubmit={this.handleSubmit} id="create-shoe-form">
                                <div className="form-floating mb-3">
                                    <input value={this.state.vin} onChange={this.handleChange} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                                    <label htmlFor="vin">Vin</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={this.state.owner_name} onChange={this.handleChange} placeholder="Owner name" required type="text" name="owner_name" id="owner_name" className="form-control" />
                                    <label htmlFor="owner_name">Owner Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={this.state.date} onChange={this.handleChange} placeholder="Date" required type="date" name="date" id="data" className="form-control" />
                                    <label htmlFor="date">Date</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={this.state.time} onChange={this.handleChange} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
                                    <label htmlFor="time">Time</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={this.state.reason} onChange={this.handleChange} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                                    <label htmlFor="reason">Reason</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <select onChange={this.handleChange} required name="technician" id="technician" className="form-select">
                                        <option value="">Choose a technician</option>
                                        {this.state.technicians.map(tech => {
                                            return (
                                                <option key={tech.id} value={tech.id}>
                                                    {tech.name}
                                                </option>
                                            );
                                        })}

                                    </select>
                                </div>
                                <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
        )
    }
}

export default AppointmentForm
