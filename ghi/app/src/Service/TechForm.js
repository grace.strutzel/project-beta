import React, { useEffect, useState } from 'react';

const TechForm = () => {

    const [techName, setTechName] = useState('');
    const [techNumber, setTechNumber] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        const newTech = {
            'name': techName,
            'employee_number': techNumber
        }

        const techUrl = 'http://localhost:8080/api/technicians/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newTech),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        fetch(techUrl, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setTechName('');
                setTechNumber('');
            })
            .catch(e => console.log('error: ', e));
    }    


    const handleNameChange = (event) => {
        const value = event.target.value;
        setTechName(value);
    }
        
    const handleNumberChange = (event) => {
        const value = event.target.value;
        setTechNumber(value);
    }



    
        return (
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create a new Technician</h1>
                            <form onSubmit={handleSubmit} id="create-shoe-form">
                                <div className="form-floating mb-3">
                                    <input value={techName} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                    <label htmlFor="name">Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={techNumber} onChange={handleNumberChange} placeholder="Number" required type="text" name="employee_number" id="employee_number" className="form-control" />
                                    <label htmlFor="employee_number">Employee Number</label>
                                </div>
                                <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    
}

export default TechForm
