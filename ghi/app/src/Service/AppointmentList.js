import React from 'react';
import {Link} from 'react-router-dom'

class AppointmentList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            appointments: []
        }
        this.handleDelete = this.handleDelete.bind(this);
        this.handleFinished = this.handleFinished.bind(this)
    }

    async componentDidMount() {
        const url = "http://localhost:8080/api/appointments/"
        const response = await fetch(url)
        if (response.ok) {
            let data = await response.json();
            this.setState({...data})
        }
    }

    async handleDelete(event) {
        const id = event.target.value
        const appUrl = `http://localhost:8080/api/appointments/${id}/`
        const fetchConfig= {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(appUrl, fetchConfig)
        if (response.ok) {
            const newAppointmentList = this.state.appointments.filter(appointment => appointment.id != appointment)
            this.setState({"appointments": newAppointmentList})
        }
    }
    catch(e) {
    }

    async handleFinished(event) {
        const id = event.target.value
        const appUrl = `http://localhost:8080/api/appointments/${id}/`
        const data = {active: true}
        const fetchConfig= {
            method: "put",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(appUrl, fetchConfig)
        if (response.ok) {
            const newAppointmentList = this.state.appointments.filter(appointment => appointment.id != appointment)
            this.setState({"appointments": newAppointmentList})
        }
    }

    render() {
        return (
            <div>
                <h1>Appointments</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Owner</th>
                            <th>Vin</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Vip</th>
                            <th>Reason</th>
                            <th>Technician</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.map(appointment => {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.owner_name}</td>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.time}</td>
                                    <td>{appointment.vip}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.technician.name}</td>
                                    <td><button onClick={this.handleFinished} value={appointment.id} className="button">Complete</button></td>
                                    <td><button onClick={this.handleDelete} value={appointment.id} className="button">Cancel</button></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <button type="button" className="btn btn-outline-info">
                    <Link to={"/appointments/new"} className="text-decoration-none">Create new Appointment</Link>
                </button>
            </div>
        )
    }
}

export default AppointmentList