import React from 'react';
import {Link} from 'react-router-dom'

class TechList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            techs: []
        }
        this.handleDelete = this.handleDelete.bind(this);
    }

    async componentDidMount() {
        const url = "http://localhost:8080/api/technicians/"
        const response = await fetch(url)
        if (response.ok) {
            let data = await response.json();
            this.setState({...data})
        }
    }

    async handleDelete(event) {
        const id = event.target.value
        const techUrl = `http://localhost:8080/api/technicians/${id}/`
        const fetchConfig= {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(techUrl, fetchConfig)
        if (response.ok) {
            const newTechList = this.state.techs.filter(tech => tech.id != id)
            this.setState({"techs": newTechList})
        }
    }
    catch(e) {
    }

    render() {
        return (
            <div>
                <h1>Technicians</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Employee</th>
                            <th>Employee Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.techs.map(tech => {
                            return (
                                <tr key={tech.id}>
                                    <td>{tech.name}</td>
                                    <td>{tech.employee_number}</td>
                                    <td><button onClick={this.handleDelete} value={tech.id} className="button">Delete</button></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <button type="button" className="btn btn-outline-info">
                    <Link to={"/technicians/new"} className="text-decoration-none">Create new Technician</Link>
                </button>
            </div>
        )
    }
}

export default TechList
