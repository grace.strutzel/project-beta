import React from 'react';
import {Link} from 'react-router-dom'

class AppointmentHistory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vin: "",
            appointments: [],
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();

        const appUrl = 'http://localhost:8080/api/appointments/<str:id>/'
        const request = await fetch(appUrl);
        if (request.ok) {
            const data = await request.json();
            this.setState({appointments: data.appointments})
        } else {
            console.log("invalid request");
        }
    }
    
    async componentDidMount() {
        const url = "http://localhost:8080/api/appointments/"
        const response = await fetch(url)
        if (response.ok) {
            let data = await response.json();
            this.setState({...data})
        } else {
            console.log("error")
        }
    }

    render() {
        return (
        
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <form className="input-group" onSubmit={this.handleSubmit}>
                                <input
                                    className="form-control" value={this.state.vin} id="vin" name="vin" maxLength={17}
                                    minLength={17} type="text"
                                />
                                <button type="button" className="btn btn-outline-info input-group-append">Search</button>
                            </form>
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th>Owner</th>
                                        <th>Vin</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Reason</th>
                                        <th>Technician</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.appointments.map(appointment => {
                                        return (
                                            <tr key={appointment.id}>
                                                <td>{appointment.owner_name}</td>
                                                <td>{appointment.vin}</td>
                                                <td>{appointment.date}</td>
                                                <td>{appointment.time}</td>
                                                <td>{appointment.reason}</td>
                                                <td>{appointment.technician.name}</td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        
                    )
                }
            }

export default AppointmentHistory