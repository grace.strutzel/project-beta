import React from 'react';
import {Link} from 'react-router-dom'

class ManufacturersList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturers: []
        }
        this.handleDelete = this.handleDelete.bind(this);
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/manufacturers/"
        const response = await fetch(url)
        if (response.ok) {
            let data = await response.json();
            this.setState({...data})
        }
    }

    async handleDelete(event) {
        const id = event.target.value
        const manufacturerUrl = `http://localhost:8100/api/manufacturers/${id}/`
        const fetchConfig= {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(manufacturerUrl, fetchConfig)
        if (response.ok) {
            const newManufacturerList = this.state.manufacturers.filter(manufacturer => manufacturer.id != id)
            this.setState({"manufacturers": newManufacturerList})
        }
    }
    catch(e) {
    }

    render() {
        return (
            <div>
                <h1>Manufacturers</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Manufacturer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.manufacturers.map(manufacturer => {
                            return (
                                <tr key={manufacturer.id}>
                                    <td>{manufacturer.name}</td>
                                    <td><button onClick={this.handleDelete} value={manufacturer.id} className="button">Delete</button></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <button type="button" className="btn btn-outline-info">
                    <Link to={"/manufacturers/new"} className="text-decoration-none">Add Manufacturer</Link>
                </button>
            </div>
        )
    }
}

export default ManufacturersList