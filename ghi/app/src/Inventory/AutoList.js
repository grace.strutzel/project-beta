import React from 'react';
import {Link} from 'react-router-dom'

class AutoList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            automobiles: []
        }
        this.handleDelete = this.handleDelete.bind(this);
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/automobiles/"
        const response = await fetch(url)
        if (response.ok) {
            let data = await response.json();
            this.setState({...data})
        }
    }

    async handleDelete(event) {
        const id = event.target.value
        const autoUrl = `http://localhost:8100/api/automobiles/${id}/`
        const fetchConfig= {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(autoUrl, fetchConfig)
        if (response.ok) {
            const newAutoList = this.state.automobiles.filter(automobile => automobile.id != id)
            this.setState({"automobiles": newAutoList})
        }
    }
    catch(e) {
    }

    render() {
        return (
            <div>
                <h1>Automobiles</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Model</th>
                            <th>Color</th>
                            <th>Year</th>
                            <th>Vin</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.automobiles.map(automobile => {
                            return (
                                <tr key={automobile.id}>
                                    <td>{automobile.model.name}</td>
                                    <td>{automobile.color}</td>
                                    <td>{automobile.year}</td>
                                    <td>{automobile.vin}</td>
                                    <td><button onClick={this.handleDelete} value={automobile.id} className="button">Delete</button></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <button type="button" className="btn btn-outline-info">
                    <Link to={"/automobiles/new"} className="text-decoration-none">Add Automobile</Link>
                </button>
            </div>
        )
    }
}

export default AutoList