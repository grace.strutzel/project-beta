import React from 'react';
import {Link} from 'react-router-dom'

class ModelsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            models: []
        }
        this.handleDelete = this.handleDelete.bind(this);
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/models/"
        const response = await fetch(url)
        if (response.ok) {
            let data = await response.json();
            this.setState({...data})
        }
    }

    async handleDelete(event) {
        const id = event.target.value
        const modelUrl = `http://localhost:8100/api/models/${id}/`
        const fetchConfig= {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(modelUrl, fetchConfig)
        if (response.ok) {
            const newModelList = this.state.models.filter(model => model.id != id)
            this.setState({"models": newModelList})
        }
    }
    catch(e) {
    }

    render() {
        return (
            <div>
                <h1>Models</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.models.map(model => {
                            return (
                                <tr key={model.id}>
                                    <td>{model.name}</td>
                                    <td>{model.manufacturer.name}</td>
                                    <td><img src={model.picture_url} /></td>
                                    <td><button onClick={this.handleDelete} value={model.id} className="button">Delete</button></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <button type="button" className="btn btn-outline-info">
                    <Link to={"/models/new"} className="text-decoration-none">Add Model</Link>
                </button>
            </div>
        )
    }
}

export default ModelsList