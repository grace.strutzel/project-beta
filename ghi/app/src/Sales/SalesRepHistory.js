// import React from 'react';



// class SalespersonHistory extends React.Component {
//     constructor(props) {
//       super(props);
//       this.state = {
//         autosales: [],
//         salesPerson: '',
//       };
//       this.handleChange = this.handleChange.bind(this);
//     }

//     async handleChange(event) {
//       const value = event.target.value;
//       this.setState({ salesPerson: value });

//       const autosaleUrl = `${process.env.REACT_APP_SALES_API}/api/autosales/`;
//       const autosaleResponse = await fetch(autosaleUrl);

//       if (autosaleResponse.ok) {
//         const autosaleData = await autosaleResponse.json();

//         if (this.state.salesPerson === "") {
//           this.setState({ autosales: autosaleData.autosales });
//         } else {
//           let autosaleByEmployee = [];
//           for (const autosale of autosaleData.autosales) {
//             if (autosale.sales_rep.sales_person === this.state.salesPerson) {
//                 autosaleByEmployee.push(autosale);
//             }
//           }
//           this.setState({ autosales: autosaleByEmployee });
//         }
//       }
//     }

//     async componentDidMount() {
//       const salespersonUrl = `${process.env.REACT_APP_SALES_API}/api/salesperson/`;
//       const autosaleUrl = `${process.env.REACT_APP_SALES_API}/api/autosales/`;
//       const salespersonResponse = await fetch(salespersonUrl);
//       const autosaleResponse = await fetch(autosaleUrl);

//       if (salespersonResponse.ok && autosaleResponse.ok) {
//         const salespersonData = await salespersonResponse.json();
//         const autosaleData = await autosaleResponse.json();

//         this.setState({
//             salesperson: salespersonData.salesperson,
//             autosales: autosaleData.autosales
//          });
//       }
//     }

//     render() {
//       return (
//         <div className="container">
//             <p></p>
//           <h2>Sales person history</h2>
//           <div className="mb-3">
//             <select onChange={this.handleChange} value={this.state.salesPerson} required name="salesPerson" id="salesPerson" className="form-select">
//               <option value="">Choose Sales Person</option>
//               {this.state.salespeople.map(salesperson => {
//                 return (
//                   <option key={salesperson.sales_person} value={salesperson.sales_person}>
//                     {salesperson.sales_person}
//                   </option>
//                 )
//               })}
//             </select>
//           </div>
//           <table className="table table-striped">
//             <thead>
//               <tr>
//                 <th>Sales Person</th>
//                 <th>Customer</th>
//                 <th>VIN</th>
//                 <th>Sale Price</th>
//               </tr>
//             </thead>
//             <tbody>
//               {this.state.autosales.map(autosale => {
//                 return (
//                   <tr key={autosale.id}>
//                     <td>{autosale.sales_rep.sales_person}</td>
//                     <td>{autosale.customer.customer_name}</td>
//                     <td>{autosale.automobile.vin}</td>
//                     <td>$ {new Intl.NumberFormat().format(autosale.price)}</td>
//                   </tr>
//                 );
//               })}
//             </tbody>
//           </table>
//         </div>
//       );
//     }
//   }

// export default SalespersonHistory;




import React from 'react';


class SalesRepHistory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            records: [],
            data_records: [],
            sales_person: []
        }

        this.handleFieldChange = this.handleFieldChange.bind(this);
    }


    async handleFieldChange(event) {
        const value = event.target.value;
        if (!value){
            this.setState({data_records: this.state.records});
        }else{
            const url_personrecord = `http://localhost:8090/api/salespersonrecords/${value}/`;
            const response = await fetch(url_personrecord);

            if (response.ok) {
                const data_records = await response.json();
                this.setState({data_records: data_records.records});
            };
        }

    };


    async componentDidMount() {
        const salespersonUrl = "http://localhost:8090/api/salesperson/";
        const response_people = await fetch(salespersonUrl);
        if (response_people.ok) {
            const data_people = await response_people.json();
            this.setState({sales_person: data_people.sales_person});
        };

        const url_records = "http://localhost:8090/api/records/";
        const response_records = await fetch(url_records);
        if (response_people.ok) {
            const data_records = await response_records.json();
            console.log(data_records)
            this.setState({data_records: data_records.sales_records});
            this.setState({records: data_records.sales_records});
        }
    }


    render() {
        return (
            <div>
                <h2 className="mt-5"><b>Sales Rep History</b></h2>
                <select value={this.state.sales_person} onChange={this.handleFieldChange} required id="sales_person" name="sales_person" className="form-select mt-3">
                    <option value="">choose a sales rep</option> #changed: show all records when choose this
                    {this.state.sales_people.map(sales_person => {
                        return (
                            <option key={sales_person.id} value={sales_person.id}>
                                {sales_person.name}
                            </option>
                        );
                    })}
                </select>

                <div>
                    <table className="table table-striped mt-3">
                        <thead>
                            <tr>
                                <th>Salesperson</th>
                                <th>Customer</th>
                                <th>VIN</th>
                                <th>Sale price</th>
                            </tr>
                        </thead>
                        <tbody id="sales_person_details">
                            {this.state.data_records.map (data => {
                                return(
                                    <tr key={data.vin.vin}>
                                        <td>{data.sales_person.name}</td>
                                        <td>{data.customer.name}</td>
                                        <td>{data.vin.vin}</td>
                                        <td>{data.price}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}


export default SalesRepHistory;
