import React, { useEffect, useState } from 'react';

const SalesPersonForm = () => {

    const [SalesRepName, SetRepName] = useState('');
    const [RepNumber, SetRepNumber] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        const newRep = {
            'name': SalesRepName,
            'employee_number': RepNumber
        }

        const salespersonUrl = 'http://localhost:8080/api/salesperson/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newRep),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        fetch(salespersonUrl, fetchConfig)
            .then(response => response.json())
            .then(() => {
                SetRepName('');
                SetRepNumber('');
            })
            .catch(e => console.log('error: ', e));
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        SetRepName(value);
    }

    const handleNumberChange = (event) => {
        const value = event.target.value;
        SetRepNumber(value);
    }
        return (
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>New Sales Rep</h1>
                            <form onSubmit={handleSubmit} id="create-shoe-form">
                                <div className="form-floating mb-3">
                                    <input value={SalesRepName} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                    <label htmlFor="name">Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={RepNumber} onChange={handleNumberChange} placeholder="Number" required type="text" name="employee_number" id="employee_number" className="form-control" />
                                    <label htmlFor="employee_number">Employee Number</label>
                                </div>
                                <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )

}

export default SalesPersonForm
