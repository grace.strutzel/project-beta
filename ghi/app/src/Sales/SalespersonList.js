import React from 'react';
import {Link} from 'react-router-dom'


class SalespersonList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sales_person: []
        };
    }

    // async componentDidMount() {
    //     const url = "http://localhost:8090/api/salesperson/";
    //     try {
    //         const response = await fetch(url);
    //         if (response.ok) {
    //             const data = await response.json();
    //             this.setState({sales_person: data.sales_person});

    //         }
    //     } catch (e) {
    //         console.error(e);
    //     }
    // }

    render () {
        return (
            <div>
                <h2 className="mt-5"><b>Sales Rep List</b></h2>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Employee Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.sales_person.map(salesperson => {
                            return (
                                <tr key={ salesperson.id }>
                                    <td>{ salesperson.name }</td>
                                    <td>{ salesperson.employee_number}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                <button type="button" className="btn btn-outline-info">
                    <Link to={"/salesperson/new"} className="text-decoration-none">Create New Salesperson</Link>
                </button>
            </div>
        )
    }
}
export default SalespersonList;
