from django.contrib import admin

# Register your models here.
from .models import Salesperson, Customer, Autosale


admin.site.register(Salesperson)
admin.site.register(Customer)
admin.site.register(Autosale)
