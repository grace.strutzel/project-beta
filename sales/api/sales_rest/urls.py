from django.urls import path
from .views import (
    api_list_customers, api_list_salesperson,
    api_show_salesperson, api_list_autosales,
    api_history_salesperson)

urlpatterns = [
    path("customers/", api_list_customers, name="api_list_customers"),
    path("salesperson/", api_list_salesperson, name="api_list_salesperson"),
    path("salesperson/<int:pk>/", api_show_salesperson, name="api_show_salesperson"),
    path("autosales/", api_list_autosales, name="api_list_autosales"),
    path("salespersonrecords/<int:pk>/", api_history_salesperson, name="api_history_salesperson"),
]
